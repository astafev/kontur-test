package ru.astafev.test.kontur.ui.console;


import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class StartConsoleTest {

    @Test
    public void testWithStandartFormat() throws Exception {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        StartConsole.processWithStandartFormat(
                StartConsoleTest.class.getClassLoader().getResourceAsStream("simpleTest.in"),
                new PrintStream(os));
        os.close();
        System.out.println(os.toString().trim());
        String expected = String.format("kanojo%nkare%nkorosu%nkaretachi%n%nkanojo%nkare%nkaretachi%n%nkare%nkaretachi%n%n");
        assertEquals(expected.trim(),
                os.toString().trim());
    }
}