package ru.astafev.test.kontur.in;

import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.*;


public class WordsReaderTest {
    WordsReader wordsReader = new WordsReader();

    @Ignore
    @Test(timeout = 10000)
    public void testRead() throws Exception {
        long t = System.currentTimeMillis();
        WordsReader.InFileReadResult result = wordsReader.read(WordsReader.getScannerFromClasspathResource("test.in"));
        t = System.currentTimeMillis() - t;
        System.out.printf("%s%n spent %d millis%n", result.tree, t);
    }

    @Test(timeout = 10000)
    public void testLongFileAndCheckRunningTime() throws Exception {
        long t = System.currentTimeMillis();
        try (Scanner scanner = WordsReader.getScannerFromClasspathResource("test.in")) {
            WordsReader.InFileReadResult result = wordsReader.read(scanner);

            for (String searchString : result.testData) {
                long t1 = System.currentTimeMillis();
                List<String> searchResult = result.tree.search(searchString, 0);
                t1 = System.currentTimeMillis() - t1;
//            System.out.println("for " + searchString + " found " + searchResult + ", spent " + t1 + " millis");
            }
            t = System.currentTimeMillis() - t;
            System.out.printf("%s%n spent %d millis%n", result.tree, t);
        }
    }

    @Test
    public void testThatChecksSimpleFile() throws Exception {
        try (Scanner scanner = WordsReader.getScannerFromClasspathResource("simpleTest.in")) {
            WordsReader.InFileReadResult result = wordsReader.read(scanner);

            assertEquals(result.testData, Arrays.asList("k", "ka", "kar"));

            assertEquals(result.tree.search("k"), Arrays.asList("kanojo", "kare", "korosu", "karetachi"));
            assertEquals(result.tree.search("ka"), Arrays.asList("kanojo", "kare", "karetachi"));
            assertEquals(result.tree.search("kar"), Arrays.asList("kare", "karetachi"));
        }
    }

    @Test
    public void testThatChecksSimpleFile2() throws Exception {
        try (Scanner scanner = WordsReader.getScannerFromClasspathResource("simpleTest2.in")) {
            WordsReader.InFileReadResult result = wordsReader.read(scanner);
            assertEquals(result.tree.search("a"), Arrays.asList("a", "aa", "aaa", "aaaa", "aaaaa"));
            assertEquals(result.tree.search("aaa"), Arrays.asList("aaa", "aaaa", "aaaaa"));
        }
    }
    @Test
    public void testThatChecksSimpleFile3() throws Exception {
        try (Scanner scanner = WordsReader.getScannerFromClasspathResource("simpleTest3.in")) {
            WordsReader.InFileReadResult result = wordsReader.read(scanner);
            assertEquals(Arrays.asList("a", "aa", "aaa", "aaaa", "aaaaa"), result.tree.search("a"));
            assertEquals(Arrays.asList("aaa", "aaaa", "aaaaa"), result.tree.search("aaa"));
        }
    }
}