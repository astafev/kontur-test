package ru.astafev.test.kontur.in;

import ru.astafev.test.kontur.model.Tree;
import ru.astafev.test.kontur.model.Word;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Consumer;

public class WordsReader {

    public static InFileReadResult read(Scanner scanner) {
        Tree tree = readTree(scanner);
        List<String> testData = readTestData(scanner);

        return new InFileReadResult(tree, testData);
    }

    public static void readAndTest(Scanner scanner, Consumer<List<String>> formatter) {
        final InFileReadResult readResult = read(scanner);
        readResult.testData.stream().forEachOrdered((searchString) ->
                        formatter.accept(readResult.tree.search(searchString))
        );
    }


    public static Tree readTree(Scanner scanner) {
        Tree root = new Tree(null);

        int N = scanner.nextInt();
        for (int i = 0; i < N; i++) {
            Word word = new Word(scanner.next(), scanner.nextInt());
            root.add(word, 0);
        }

        return root;
    }

    public static List<String> readTestData(Scanner scanner) {
        int M = scanner.nextInt();
        List<String> testData = new ArrayList<>(M);
        for (int i = 0; i < M; i++) {
            testData.add(scanner.next());
        }
        return testData;
    }

    public static Scanner getScannerFromClasspathResource(String name) {
        return new Scanner(WordsReader.class.getClassLoader().getResourceAsStream(name));
    }

    public static Scanner getScannerFromFile(String filePath) throws FileNotFoundException {
        return new Scanner(new File(filePath));
    }

    public static class InFileReadResult {
        public final Tree tree;
        public final List<String> testData;

        public InFileReadResult(Tree tree, List<String> testData) {
            this.tree = tree;
            this.testData = testData;
        }
    }

}
