package ru.astafev.test.kontur.model;

import java.util.*;

public class Index {
    static final int MAX_IN_INDEX = 10;

    private int lowest = -1;
    private LinkedList<Word> list = new LinkedList<>();


    public void add2index(Word word2add) {
        boolean addedInMiddle = false;
        if (word2add.rate >= lowest) {
            ListIterator<Word> wordIterator = list.listIterator();
            while (wordIterator.hasNext()) {
                Word next = wordIterator.next();
                if (next.compareTo(word2add) <= 0) {
                    addedInMiddle = true;
                    wordIterator.previous();
                    wordIterator.add(word2add);
                    if (list.size() > MAX_IN_INDEX) {
                        list.removeLast();
                    }
                    lowest = list.getLast().rate;
                    break;
                }
            }
        }
        if(!addedInMiddle && list.size()<MAX_IN_INDEX) {
            list.addLast(word2add);
            lowest = word2add.rate;
        }
    }

    public List<Word> list() {
        return Collections.unmodifiableList(list);
    }
    public List<String> stringList() {
        ArrayList<String> strings = new ArrayList<>(list.size());
        for(Word word:list) {
            strings.add(word.name);
        }
        return Collections.unmodifiableList(strings);
    }
}
