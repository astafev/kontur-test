package ru.astafev.test.kontur.model;

public final class Word implements Comparable<Word> {
    public final String name;
    public final int rate;

    public Word(String name, int rate) {
        if (name == null)
            throw new NullPointerException("name can't be null");
        this.name = name;
        this.rate = rate;
    }

    @Override
    public int compareTo(Word o) {
        int rateComparison = Integer.compare(rate, o.rate);
        if (rateComparison == 0)
            return -name.compareTo(o.name);
        else return rateComparison;
    }

    @Override
    public String toString() {
        return name + " " + rate;
    }
}