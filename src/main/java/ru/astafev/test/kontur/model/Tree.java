package ru.astafev.test.kontur.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * TODO ������� ��� ���
 * */
public class Tree {
    private final Character key;

    HashMap<Character, Tree> subtrees = new HashMap<>();
    Index index = new Index();

    public Tree(Character key) {
        this.key = key;
    }

    public void add(Word word, int level) {
        index.add2index(word);
        if(level == word.name.length())
            return;
        Character ch = word.name.charAt(level);
        Tree subtree = subtrees.get(ch);

        if (subtree == null) {
            subtree = new Tree(ch);
            subtrees.put(ch, subtree);
        }
        ++level;
        if (level <= word.name.length())
            subtree.add(word, level);
    }

    public List<String> search(String searchString) {
        return search(searchString, 0);
    }
    public List<String> search(String searchString, int level) {
        if (level == searchString.length())
            return index.stringList();

        Character ch = searchString.charAt(level);
        Tree subtree = subtrees.get(ch);
        if (subtree == null)
            return Collections.emptyList();

        return subtree.search(searchString, ++level);
    }

    @Override
    public String toString() {
        return index.list() + "\n " + subtrees.size() + " subtrees";
    }
}
