package ru.astafev.test.kontur.ui.console;

import ru.astafev.test.kontur.in.WordsReader;
import ru.astafev.test.kontur.ui.StandartOutFormat;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class StartConsole {
    public static void main(String[] args) {
        processWithStandartFormat(System.in, System.out);
    }


    static void processWithStandartFormat(InputStream stdin, PrintStream stdout) {
        Scanner scanner = new Scanner(stdin);
        WordsReader.readAndTest(scanner, new StandartOutFormat(stdout));
    }
}
