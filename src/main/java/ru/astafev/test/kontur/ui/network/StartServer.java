package ru.astafev.test.kontur.ui.network;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.astafev.test.kontur.in.WordsReader;
import ru.astafev.test.kontur.model.Tree;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class StartServer {
    public static Logger log = LoggerFactory.getLogger(StartServer.class);

    public static final String USAGE =
            "-f file\n" +
                    "-p port\n" +
                    "     default = 4444";

    static int port = 4444;
    static String file = null;


    public static void main(String[] args) throws IOException {

        try {
            parseArgs(args);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(USAGE);
            return;
        }

        checkState();
        final Tree wordsTree;
        try (Scanner scanner = WordsReader.getScannerFromFile(file)) {
            wordsTree = WordsReader.readTree(scanner);
        }

        ExecutorService executor = Executors.newCachedThreadPool();

        ServerSocket serverSocket = new ServerSocket(port);
        log.info("launched server on port " + port);
        while (true) {
            Socket socket = serverSocket.accept();
            log.debug("got connection");
            Runnable connectionWorker = () -> {
                try {
                    ProtocolImpl protocol = new ProtocolImpl(socket);
                    a:
                    while (true) {
                        try {
                            ProtocolImpl.Command command = protocol.getCommand();
                            log.debug("got command {}", command);
                            if (command != null) {
                                switch (command.commandType) {
                                    case GET:
                                        command.execute(wordsTree);
                                        break;
                                    case QUIT:
                                        log.debug("quit");
                                        break a;
                                }
                            }
                        } catch (IllegalArgumentException e) {
                            log.warn("wrong command", e);
                        }
                    }
                } catch (java.net.SocketException e) {
                    log.warn("connection probably closed", e);
                } catch (IOException e) {
                    log.error("", e);
                }
            };
            executor.execute(connectionWorker);
        }
    }

    private static void checkState() {
        if (file == null) {
            throw new IllegalStateException("file wasn't set");
        }
    }

    private static void parseArgs(String[] args) {
        //�� ���� ��� ������ �������� ���������, �� �� ����� ����� �������
        if (args.length == 1) {
            file = args[0];
            return;
        }
        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case "-p":
                    port = Integer.parseInt(args[++i]);
                    break;
                case "-f":
                    file = args[++i];
                    break;
                default:
                    throw new IllegalArgumentException("unknown argument: " + args[i]);
            }
        }
    }
}
