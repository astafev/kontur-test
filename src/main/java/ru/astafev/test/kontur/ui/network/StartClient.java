package ru.astafev.test.kontur.ui.network;

import ru.astafev.test.kontur.ui.StandartOutFormat;

import java.io.*;
import java.util.List;

public class StartClient {
    public static final String USAGE =
            "-h host\n" +
                    "    default <localhost>" +
                    "-h port\n" +
                    "    default <4444>";
    static String host = "localhost";
    static int port = 4444;

    public static void main(String[] args) throws IOException {
        ProtocolImpl protocol = new ProtocolImpl(host, port);
        try{
            parseArgs(args);
        }catch (Exception e) {
            System.err.println(USAGE);
            e.printStackTrace();
            return;
        }
        String word;
        StandartOutFormat outFormat = new StandartOutFormat(System.out);
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            word = in.readLine();
            if ("\\q".equals(word)) {
                protocol.sendQuitCommand();
                break;
            }

            List<String> searchResult = protocol.sendGetCommand(word);
            outFormat.accept(searchResult);
        }
    }


    private static void parseArgs(String[] args) {
        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case "-p":
                    port = Integer.parseInt(args[++i]);
                    break;
                case "-h":
                    host = args[++i];
                    break;
                default:
                    throw new IllegalArgumentException("unknown argument: " + args[i]);
            }
        }
    }

}
