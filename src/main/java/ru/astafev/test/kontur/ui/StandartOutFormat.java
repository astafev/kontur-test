package ru.astafev.test.kontur.ui;

import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.function.Consumer;

public class StandartOutFormat implements Consumer<List<String>> {
    PrintStream stdout;
    PrintWriter writer;

    public StandartOutFormat(PrintStream stdout) {
        this.stdout = stdout;
    }

    public StandartOutFormat(PrintWriter stdout) {
        this.writer = stdout;
    }

    @Override
    public void accept(List<String> words) {
        for (String w : words) {
            if (stdout != null)
                stdout.println(w);
            else writer.println(w);
        }
        if (stdout != null)
            stdout.println();
        else writer.println();
    }
}
