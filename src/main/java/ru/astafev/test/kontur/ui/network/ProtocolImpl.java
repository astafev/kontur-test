package ru.astafev.test.kontur.ui.network;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.astafev.test.kontur.model.Tree;
import ru.astafev.test.kontur.ui.StandartOutFormat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ProtocolImpl {
    private final Socket socket;
    BufferedReader in;
    PrintWriter out;

    public static Logger log = LoggerFactory.getLogger(ProtocolImpl.class);

    private final String COMMAND_SPLITTER = " ";

    public ProtocolImpl(String host, int port) throws IOException {
        this(new Socket(host, port));
    }
    public ProtocolImpl(Socket socket) throws IOException {
        this.socket = socket;
        in = new BufferedReader(
                new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);
    }

    public Command getCommand() throws IOException {
        String commandLine = in.readLine();
        String[] commandParts = commandLine.split(COMMAND_SPLITTER);
        switch (commandParts[0]) {
            case "get":
                if(commandParts.length != 2) {
                    out.printf("wrong format");
                    throw new IllegalArgumentException("wrong format");
                }
                return new GetCommand(commandParts[1]);
            case "quit":
                return new QuitCommand();
            default:
                throw new UnsupportedOperationException("unknown command: " + commandParts[0]);
        }
    }

    public List<String> sendGetCommand(String word) throws IOException {
        out.printf("get %s%n", word);
        String line;
        List<String> result = new ArrayList<>();
        while (!"".equals(line = in.readLine())) {
            result.add(line);
        }
        return result;
    }
    public void sendQuitCommand() throws IOException {
        out.printf("quit%n");
        out.close();
    }

    public abstract class Command {
        protected final CommandType commandType;
        protected String command;

        protected Command(CommandType commandType) {
            this.commandType = commandType;
        }
        public Command(CommandType commandType, String command) {
            this.commandType = commandType;
            this.command = command;
        }

        public abstract void execute(Object... args) throws IOException;

        @Override
        public String toString() {
            return "Command{" +
                    "commandType=" + commandType +
                    ", command='" + command + '\'' +
                    '}';
        }
    }

    public class GetCommand extends Command {

        protected GetCommand(String word) {
            super(CommandType.GET, word);
        }

        /**
         * @param args: [0] - Tree
         * */
        @Override
        public void execute(Object... args) {
            Tree tree = (Tree) args[0];
            List<String> search = tree.search(command);
            log.debug("result of get command: " + search);
            new StandartOutFormat(out).accept(search);
        }


    }

    public enum CommandType {
        GET, QUIT
    }

    public class QuitCommand extends Command {

        protected QuitCommand() {
            super(CommandType.QUIT);
        }

        @Override
        public void execute(Object... args) throws IOException {
            socket.close();
        }
    }
}
